# -*- coding: utf-8 -*-
import scrapy
from scrapy_splash import SplashRequest


class SpotheroratesSpider(scrapy.Spider):
    name = 'spotherorates'
    allowed_domains = ['https://spothero.com/boston-parking']
    def start_requests(self):
        url = ["https://spothero.com/boston-parking"]
        for url in url:
            yield SplashRequest(url=url, callback = self.parse)

    def parse(self, response):
        try:
            address = response.xpath(
                ".//span[@itemprop='address']/meta[@itemprop='streetAddress']/@content").extract_first()

        except AttributeError:
            address = response.xpath(
                ".//div[@class='detail']/h2/span/p[@class='TransientMonthlyFacilityDetails-title']/*").extract()

        # print("procesing:" + response.url)
        # row_data = zip(address)
        #
        # for item in row_data:
        #     # create a dictionary to store the scraped info
        #     scraped_info = {
        #         # key:value
        #         'page': response.url,
        #         'address': item[0]  # item[0] means product in the list and so on, index tells what value to assign
        #         #                 'price_range' : item[1],
        #         #                 'orders' : item[2],
        #         #                 'company_name' : item[3],
        #     }
        #
        #     # yield or give the scraped info to scrapy
        yield {"Location": address}

