# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository consist of two python files used for scraping garage data, prices and address. Scraping is done on two websites: parkwhiz.com and spothero.com. File naming convention are based on the website names.

### How do I get set up? ###
For Parkwhiz, you can run the python script in Pycharm IDE or Jupyter Notebook.

For spotherorates, you will need to run the code in the command line.
Follow this website to run code in command line: https://www.datacamp.com/community/tutorials/making-web-crawlers-scrapy-python

**CMD code**

scrapy startproject foldername

scrapy genspider spotherorates (website link to scrape)
//This creates a python file to edit and write the code that I have shared.(copy paste the code from spotherorates.py in this file)


scrapy crawl spotherorates

### Contribution guidelines ###

spothero website, i was not able to scrape as it had javascript embedded content. I tried different methods to scrape it but failed to scrape the website.
