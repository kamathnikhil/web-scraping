import pandas as pd
import numpy as np
from selenium import webdriver
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as uReq
import requests
import csv
import time

my_url = "https://www.parkwhiz.com/p/east-boston-parking/map/"

uClient = uReq(my_url)
page_html = uClient.read()
uClient.close()
page_soup = soup(page_html,"html.parser")

containers = page_soup.findAll("div",{"class": "listing"})

print(len(containers))

print(soup.prettify(containers[0]))


# In[23]:


container = containers[0]


# In[2]:


address_container = container.findAll("div", {"class": "address"})
address = address_container[0].text.strip()
address


# In[113]:


price_container = container.findAll("div", {"class": "col-xs-4 col-gutter-left-0"})
price = price_container[0].text.strip()
price


# In[114]:


garage_container = container.findAll("div", {"class": "location-name"})
garage = garage_container[0].text
garage


# In[117]:


#storing the csv files parkwhiz
filename = "parkwhizgarageFenway.csv"
f = open(filename,"w")

headers = "node_type, name, address, price\n"
f.write(headers)

for container in containers:
    garage_container = container.findAll("div", {"class": "location-name"})
    garage = garage_container[0].text
    
    price_container = container.findAll("div", {"class": "col-xs-4 col-gutter-left-0"})
    price = price_container[0].text.strip()
    
    address_container = container.findAll("div", {"class": "address"})
    address = address_container[0].text
    
    #removed $ symbol from the data, as I wanted numeric data in my df
    trim_price = "".join(price.split("$"))
    
    node_type = "CG"
    
    print("name:" + garage)
    print("price:" + trim_price)
    print("address:" + address)
    print("node_type:" + node_type)
    
    f.write(node_type+","+garage+","+address+","+trim_price+"\n")

f.close()

